import { useState} from "react";
import AddModel from "./AddModel.js";
import ModelList from "./ModelList.js";
import FormList from "./FormList.js";
import modelsData from './model_info.json';

let nextId = 1;

export default function Sidebar() {
  const [models, setModels] = useState(modelsData);
  const [mields, setMields] = useState([]);
  const [checks, setChecks] = useState([]);
  const [data, setData] = useState([]);

  function handleAddModel(bool) {
    setModels([
      ...models,
      {
        id: nextId++,
        title: "Model",
        done: bool,
      },
    ]);
  }
  function handleAddMields(fields) {
    setMields([
      ...mields,
      {
        mid: fields.mid,
        id: fields.id,
        title: fields.title,
        type: fields.type,
        value: fields.value,
        attrs: fields.attrs ? fields.attrs : null
      },
    ]);
  }

  function handleChangeModel(nextModel) {
    setModels(
      models.map((t) => {
        if (t.id === nextModel.id) {
          if(nextModel.done){
            if(checks.length < 2) {
              setChecks([...checks, nextModel.id]);
              return nextModel;
            }
            else return t;
          }
          else {
            setChecks(checks.filter(item => item !== nextModel.id));
            return nextModel;
          }
        } else {
          return t;
        }
      })
    );
  }
  function handleChangeMields(nextMield) {
    setMields(
      mields.map((t) => {
        if (t.id === nextMield.id) return nextMield;
        else return t;
      })
    );
  }

  function handleDeleteModel(modelId) {
    setModels(models.filter((t) => t.id !== modelId));
    setChecks(checks.filter(item => item !== modelId));
  }
  function handleDeleteMields(mId, fid) {
    setMields(mields.filter((t) => t.mid !== mId && t.id !== fid));
  }
  
  function handleDuplicate(fields) {
    handleAddModel(true);
    setChecks([...checks, nextId-1]);
    setData(fields);
  };
  
  return (
    <div className="grid grid-cols-3 gap-3">
      <div>
        <ModelList
          models={models}
          onDeleteModel={handleDeleteModel}
          onChangeModel={handleChangeModel}
        />
        <br/>
        <AddModel onAddModel={handleAddModel} />
      </div>
  
      <FormList
        checks={checks}
        onDuplicate={handleDuplicate}
        data={data}
        models={models}
        mields={mields}
        onA={handleAddMields}
        onC={handleChangeMields}
        onD={handleDeleteMields}
      />
    </div>
  );
}
