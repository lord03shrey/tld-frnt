export default function ModelList({
  models,
  onChangeModel,
  onDeleteModel
}) {
  
  return (
    <ul>
      {models.map(model => (
        <li key={model.id}>
          <Task
            model={model}
            onDelete={onDeleteModel}
            onChange={onChangeModel}
          />
        </li>
      ))}
    </ul>
  );
}

function Task({ model, onDelete, onChange }) {
  return (
    <label>
      <input
        type="checkbox"
        checked={model.done}
        onChange={e => {
          onChange({
            ...model,
            done: e.target.checked
          });
        }}
      />
      {'Model '}{model.id}&emsp;
      <button onClick={() => onDelete(model.id)}>
        🗑️
      </button>
    </label>
  );
}