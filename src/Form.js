import { useState, useEffect, useRef } from 'react';
import AddField from './AddField.js';
import FieldList from './FieldList.js';

let nextId = 0;

export default function Form({ id, onDuplicate, dup, data, onTransfer, a, models, mields, onA, onC, onD }) {
  const [fields, setFields] = useState(data);
  const [pk, setPk] = useState(false);
  const [e, setE] = useState({title: '', type: '', value: ''})
  const inputRef = useRef(null);
  
  let idli = id;
  useEffect(() => {
    if (a !== null) {
      handleAddField(null, a.title, a.type, a.value);
    }
  }, [a]);
  
  function handleAddField(id, title, type, value, attr) {
    var fk = false;
    if(type === 'fk'){
      models.some((model) => model.id == value) ? fk = true: fk = false;
    }
    
    setE('')
    if(title !== '' && value !== '' && ((type === 'fk' && fk === true)||(type !== 'fk' && fk === false)) ){
      const cid = (id === '')? nextId++ : id;
      setFields((fields) => {
        return [
          ...fields,
          {
            mid: idli,
            id: cid,
            title: title,
            type: type,
            value: value
          }
        ]        
      });
      onA({mid: idli, id: cid, title: title, type: type, value: value, attr: attr});
      if(type === 'pk')setPk(true);
      console.log(fields)
    }
  }

  function handleDeleteField(fieldId) {
    if(fields.filter(t => t.id === fieldId)[0].type === 'pk') setPk(false);
    setFields(
      fields.filter(t => t.id !== fieldId)
    );
    onD(idli, fieldId);
  }

  const handleDuplicate = () => {
    onDuplicate(fields);
  };

  function handleEdit(field) {
    setE(field)
  };
  
  function handleSaveButtonClick() {
    const jsonData = JSON.stringify(fields);
    const blob = new Blob([jsonData], { type: 'application/json' });
    const downloadLink = document.createElement('a');
    downloadLink.href = URL.createObjectURL(blob);
    downloadLink.download = 'Model_'+id+'.json';
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };
 
  return (
    <>
      Model: &nbsp;{id}
      &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
      {dup? (<button onClick={handleDuplicate}>📖</button>):(<></>)}
      <br /><br />
      <FieldList
        fields={fields}
        onDeleteField={handleDeleteField}
        onTransfer={onTransfer}
        a={a}
        onAdd={handleAddField}
        onEdit={handleEdit}
        focusInput={inputRef}
      />
      _________________________________________
      <br/>
      <AddField
        onAddField={handleAddField}
        e={e}
        inputRef={inputRef}
        pk={pk}
        models={models}
        mields={mields}
        onA={onA}
        onC={onC}
        onD={onD}
      /> &emsp;&emsp;&emsp;
      <button onClick={handleSaveButtonClick}>💾</button>

    </>
  );
}

