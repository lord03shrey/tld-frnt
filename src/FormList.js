import Form from './Form.js';
import {useState} from 'react'
export default function FormList({ checks, onDuplicate, data, models, mields, onA, onC, onD }) {
  const [a, setA] = useState(null)
  function handleTransfer(field){
    setA(field)
  }
  if(checks.length === 2){
    return (
      <>
        <div className="border-solid border-2 border-black" align="center">
          <Form id={checks[0]} onDuplicate={onDuplicate} dup={false} data={[]} onTransfer={handleTransfer} a={null} models={models} mields={mields} onA={onA} onD={onD} onC={onC} />
        </div>
        <div className="border-solid border-2 border-black" align="center">
          <Form id={checks[1]} onDuplicate={onDuplicate} dup={false} data={data} onTransfer={handleTransfer} a={a} models={models} mields={mields} onA={onD} onB={onD} onC={onC} />
        </div>
      </>
    );
  }
  else if(checks.length === 1) {
    return (
      <div className="border-solid border-2 border-black" align="center">
        <Form id={checks[0]} onDuplicate={onDuplicate} dup={true} data={[]} onTransfer={handleTransfer} a={null} models={models} mields={mields} onA={onA} onD={onD} onC={onC} />
      </div>
    );
  }
  else {
    return (
      <>
      </>
    );
  }

}
