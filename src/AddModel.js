export default function AddModel({onAddModel}) {
  return (
    <>
      <button onClick={() => {
        onAddModel(false);
      }}>Add ➕</button>
    </>
  )
}