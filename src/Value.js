import { useState } from 'react';

export default function Value({ onAddField }) {
  const [title, setTitle] = useState('');
  const [type, setType] = useState('text');
  const [value, setValue] = useState('');
  
  const handleDataTypeChange = (event) => {
    setType(event.target.value);
    setValue(''); // Reset the input value when data type changes
  };

  const handleInputChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <>
      <input
        placeholder="Add field"
        value={title}
        onChange={e => setTitle(e.target.value)}
      />
      
      <br/>
      
      <label htmlFor="input type">Choose:&emsp;</label>
      <select id="choices" value={type} onChange={handleDataTypeChange}>
        <option value="number">Integer</option>
        <option value="text">Text</option>
        <option value="boolean">Boolean (y/n)</option>
        <option value="password">Password</option>
        <option value="email">Email</option>
        <option value="date">Date</option>
      </select>

      <br/>
      
      {type === 'boolean' ? (
        <div>
          <label>
            <input
              type="radio"
              name="booleanValue"
              value="Yes"
              checked={value === 'Yes'}
              onChange={handleInputChange}
            /> Yes
          </label>
          <label>
            <input
              type="radio"
              name="booleanValue"
              value="No"
              checked={value === 'No'}
              onChange={handleInputChange}
            /> No
          </label>
        </div>
      ) : (
        <input
          type={type}
          value={value}
          onChange={handleInputChange}
          placeholder={`Enter ${type} value`}
        />
      )}
      
      <button onClick={() => {
        setTitle('')
        setType('text')
        setValue('')
        onAddField(title, type, value)
      }}>Add ➕ </button>
      
    </>
  )}