import { useState } from 'react';

export default function FieldList({
  fields,
  onDeleteField,
  onTransfer,
  a,
  onAdd,
  onEdit,
  focusInput,
}) {
  return (
  <ul>
    <div className="flex flex-col h-72 overflow-auto">
    <li>
      {fields.map(field => (
        <div key={field.id}>
          <Task
            field={field}
            onDelete={onDeleteField}
            onTransfer={onTransfer}
            a={a}
            onAdd={onAdd}
            onEdit={onEdit}
            focusInput={focusInput}
          />
        </div>
      ))}
    </li>
    </div>
  </ul>
  );
}

function Task({ field, onDelete, onTransfer, a, onAdd, onEdit, focusInput}) {
  let fieldContent;
  
  const handleEdit = () => {
    fieldContent = field
    if (focusInput.current) {
      focusInput.current.focus();
    }
    onDelete(field.id)
    onEdit(fieldContent)
  };
  
  const handleTransfer = () => {
    onTransfer(field);
  };
  
  return (
    <>
      <div style={{width: '240%', margin: '-2px', maxWidth: '345px' }}>
        <div className="border-solid border-2 border-black" style={{ display: 'inline-block', width: '30%', overflow: 'hidden' }} >
          {field.title}
        </div>
      
        <div className="border-solid border-2 border-black" style={{ display: 'inline-block', width: '30%', overflow: 'hidden' }} >
          {field.type}
        </div>
        
        <div style={{ display: 'inline-block', width: '40%', overflow: 'hidden' }} >
          <button onClick={handleEdit}>
            📝
          </button>
          &emsp;
          <button onClick={() => onDelete(field.id)}>
            ❌
          </button>
          &emsp;
          <button onClick={() => onAdd('', field.title, field.type, field.value)}>
            ↪️
          </button>
          &emsp;
          {a === null? (
            <button onClick={handleTransfer}>
              ▶️
            </button>
          ): (<></>)}
        </div>   
      </div>
    </>
  );
}