import { useState } from 'react';

export default function ModalFK({ models, ModalDisplay, mields, onA, onC, onD }) {
  const [attr, setAttr] = useState({
    id: models[0].id,
    title: '',
    type: '',
    attrs: {},
    on_delete: ''
  });

  const closeModal = () => {
    ModalDisplay(true)
    console.log(mields)
  };

  function handleSave() {
    const r = mields.filter(mield => mield.mid === attr.id)
    const f = {
      mid: attr.id,
      id: r[r.length - 1].id + 1,
      title: attr.title,
      type: 'fk',
      attrs: {...attr.attrs, on_del: attr.on_delete}
    }
    onA(f)
    closeModal();
  } 

  const handleModelChange = (event) => {
    setAttr({
      ...attr,
      id: event.target.value,
      attrs: {}});
  };
  const handleDelChange = (event) => {
    setAttr({
      ...attr,
      on_delete: event.target.value,
      attrs: {}});
  };
  const handleTitleChange = (event) => {
    setAttr({
      ...attr,
      title: event.target.value,
      attrs: {}});
    
    handleAttrs(mields.filter(mield => mield.mid == attr.id && mield.title == event.target.value )[0].type, 255)
  };

  function handleAttrs(type, val) {
    console.log(type)
    if(type == 'text') setAttr({
      ...attr,
      attrs: {max_length: val}
    })
    if(type == 'email') setAttr({
      ...attr,
      attrs: {max_length: val}
    })
    if(type == 'textarea') setAttr({
      ...attr,
      attrs: {max_length: val}
    })
  }
  
  return (
<div className="relative z-10" aria-labelledby="modal-title" role="dialog" aria-modal="true">
  <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

  <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
    <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
      <div className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
        <div className="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
          <div className="sm:flex sm:items-start">
            <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
              <h3 className="text-base font-semibold leading-6 text-gray-900" id="modal-title">Foreign Key</h3>

              <label htmlFor="input type">Model:&emsp;</label>
              <select id="choices" value={attr.id} onChange={handleModelChange}>
                {models.map(model => 
                  <option value={model.id}>Model {model.id}</option>
                  )
                }
              </select>
              <label htmlFor="input type">&emsp;Field:&emsp;</label>
              <select id="choices" value={attr.title} onChange={handleTitleChange}>
                {mields.map(mield => mield.mid == attr.id?
                  <option value={mield.title}>{mield.title}</option> : <></>
                  )
                }
              </select>
              <br/>
              {attr.attrs.max_length > 0 ? (<>
                <label htmlFor="input type">Max_length:&emsp;</label>
                <input value={attr.attrs.max_length} type="number" min={0} /> </>) : <></>
              }
              <label htmlFor="input type">on_delete:&emsp;</label>
              <select id="choices" value={attr.on_delete} onChange={handleDelChange}>
                <option value={`CASCADE`}>models.CASCADE</option>
                <option value={`PROTECT`}>models.PROTECT</option>
                <option value={`SET_NULL`}>models.SET_NULL</option>
                <option value={`DO_NOTHING`}>models.DO_NOTHING</option>
              </select>
              
            </div>
          </div>
        </div>
        <div className="bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
          <button type="button" onClick={handleSave} className="inline-flex w-full justify-center rounded-md bg-green-500 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-green-400 sm:ml-3 sm:w-auto">Save</button>
          <button type="button" onClick={closeModal} className="inline-flex w-full justify-center rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 sm:ml-3 sm:w-auto">❌</button>
        </div>
      </div>
    </div>
  </div>
</div>
  );
};
  