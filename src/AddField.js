import { useState, useEffect } from 'react';
import { v4 } from 'uuid';
import ModalFk from './ModalFk.js';

export default function AddField({ onAddField, e, inputRef, pk, models, mields, onA, onC, onD }) {
  let buf;
  const [id, setId] = useState(1);
  const [title, setTitle] = useState('');
  const [type, setType] = useState('text');
  const [value, setValue] = useState('');
  if(e.title !== '') buf = e.id;
  else buf = id;
  useEffect(() => {
    if(e.title !== '') setTitle(e.title);
    if(e.value !== '') setValue(e.value);
    if(e.type !== '') setType(e.type);
  }, [e.title, e.value, e.type]);
  
  const handleDataTypeChange = (event) => {
    setType(event.target.value);
    setValue(''); 
  };

  const handleInputChange = (event) => {
    setValue(event.target.value);
  };
  
  function handleModalDisplay(event) {
    if(event) setType('charString');
  };
  
  const uuid = () => {
    setValue(v4());
  };
  
  return (
    <>      
      <input
        value={buf}
        onChange={e => setId(e.target.value)}
        hidden={true}
      />
      
      <input
        placeholder="Add field"
        value={title}
        onChange={e => setTitle(e.target.value)}
        ref={inputRef}
      />
      
      <br/>
      
      <label htmlFor="input type">Choose:&emsp;</label>
      <select id="choices" value={type} onChange={handleDataTypeChange}>
        <option value="number">Integer</option>
        <option value="text">charString</option>
        <option value="boolean">Boolean (y/n)</option>
        <option value="password">Password</option>
        <option value="email">Email</option>
        <option value="date">Date</option>
        <option value="textarea">Text</option>
        {pk === false? (<option value="pk">AutoPK</option>) : (<></>)}
        <option value="fk">FK</option>        
      </select>

      <br/>
      
      {type === 'boolean' ? (
        <div>
          <label>
            <input
              type="radio"
              name="booleanValue"
              value="True"
              checked={value === 'True'}
              onChange={handleInputChange}
            /> True
          </label>
          <label>
            <input
              type="radio"
              name="booleanValue"
              value="False"
              checked={value === 'False'}
              onChange={handleInputChange}
            /> False
          </label>
        </div>
      ) : type === 'textarea' ? (
        <textarea
          value={value}
          onChange={handleInputChange}
          placeholder={`Enter text`}
          rows={5}
          cols={30}
        />
      ) : type === 'pk' ? (
        <input
          type={`text`}
          value={value}
          onFocus={uuid}
          placeholder={`Autogenerate pk`}
        />
      ) : type === 'fk' ? (
        <ModalFk
          models={models}
          ModalDisplay={handleModalDisplay}
          mields={mields}
          onA={onA}
          onC={onC}
          onD={onD}
        />
      ) : (
        <input
          type={type}
          value={value}
          onChange={handleInputChange}
          placeholder={`Enter ${type} value`}
        />
      )}

      <br/>
      
      <button onClick={() => {
        onAddField(id, title, type, value)
        setId(id+1)
        setTitle('')
        setType('text')
        setValue('')
      }}>Add ➕ </button>
    </>
  )}